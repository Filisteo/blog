<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('language');

Route::get('/', [App\Http\Controllers\BlogController::class,'index'])->middleware('language');
Route::get('/blog', [App\Http\Controllers\BlogController::class,'index'])->name('blog');
Route::get('/search', [App\Http\Controllers\BlogController::class,'search'])->name('search');
Route::get('/category/{category}', [App\Http\Controllers\BlogController::class,'category'])->name('category');
Route::get('/tag/{tag}', [App\Http\Controllers\BlogController::class,'tag'])->name('tag');
Route::get('/showPost/{post}', [App\Http\Controllers\BlogController::class,'showPost'])->name('showPost');

Auth::routes(['verify' => 'true']);

//Para poder editar, precisas del rol editor, y estar verificado
Route::middleware(['auth' => 'verified', 'language', 'can:editor'])->controller(App\Http\Controllers\BlogController::class)->group(function(){
    Route::get('/create', 'create')->name('create');
    Route::post('posts','store')->name('store');
    Route::get('/showUserPosts/{name}', 'showUserPosts')->name('showUserPosts');
    Route::get('/showPost/{post}/edit', 'edit')->name('edit');
    Route::put('/blog/{post}', 'update')->name('update');
    Route::delete('/post/{post}', 'destroy')->name('destroy');
});


