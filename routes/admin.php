<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\Admin\UserController;

//La ruta admin, contiene todas las rutas cuyos métodos son propios de el controlador admin
//la directiva de can es un directiva que permite conocer el rol de un usuario, como middleware, un usuario no puede manualmente
//acceder a las rutas de los administradores.
Route::middleware('can:admin')->group(function(){

    Route::get('', [HomeController::class, 'index'])->name('admin.home');

    // Solo necesito acceder a las tres rutas
    Route::resource('users', UserController::class)->only(['index','edit','update'])->names('admin.users');
    
    Route::resource('categories', CategoryController::class)->names('admin.categories');
    
    Route::resource('tags', TagController::class)->names('admin.tags');
});