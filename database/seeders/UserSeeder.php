<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Utilizo el seeder de User para generarme automáticamente un suaurio
        User::create([
            'name' => 'gabriel',
            'email' => 'gabrielcorreiaferrera2001@gmail.com',
            'id' => 1,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => Str::random(10),
            'email_verified_at' => now(),
        ])->assignRole('Admin');

        User::factory(5)->create()->each(function($user){
            $user->assignRole('Editor');
        });
    }
}
