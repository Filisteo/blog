<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RollSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    //CREANDO LOS ROLES
    //Con la libreria Spatie de roles, soy capaz de crear un rol, y asociar ese rol a una privilegio o permiso
    public function run()
    {
        $role_admin = Role::create(['name' => 'Admin']);
        $role_editor = Role::create(['name' => 'Editor']);

        Permission::create(['name' => 'admin'])->assignRole($role_admin);
        Permission::create(['name' => 'editor'])->syncRoles($role_editor, $role_admin);
        
    }
}
