@section('content')
    <main class="mx-5 my-2">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        @if (session('updated'))
            <div class="alert alert-warning">
                {{ session('updated') }}
            </div>
        @endif

        {{-- <div class="d-flex mb-4">
            <div class="mr-4">
                <a class="text-light text-decoration-none rounded bg-primary p-2 mr-4">
                    Show Most popular
                </a>
            </div>
            <div>
                <a class="text-light text-decoration-none rounded  bg-primary p-2 ms-3">
                    Show Most recent
                </a>
            </div>
        </div> --}}

        @foreach ($posts as $post)
            <div class="container">
                <div class="row">
                    <div class="col-4 rounded" style="
                        width: 400px;
                        height:148px;
                        background-image: url({{ $post->image }});
                        background-size: cover;
                        background-repeat:no-repeat">
                    </div>
                    <div class="col-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <a href="{{ route('showPost', $post) }}">
                                    <h5 class="card-title">{{ $post->title }}</h5>
                                </a>
                                <p class="flex-grow-1 card-text me-5">
                                    {{ $post->description }}
                                </p>
                                <p class="card-text">
                                <div class="row">
                                    <div class="col row">
                                        <small class="col col-xs">Tags:
                                            @foreach ($post->tags as $tag)
                                                <a href="{{ route('tag', $tag) }}"
                                                    class="btn btn-{{ $tag->color }} btn-sm rounded-pill">{{ $tag->name }}</a>
                                            @endforeach
                                        </small>
                                    </div>
                                    <div class="col row">
                                        <small>Category:
                                            @foreach ($categories as $category)
                                                @if ($post->category->id == $category->id)
                                                    <a href="{{ route('category', $category) }}"
                                                        class="btn btn-primary btn-sm rounded-pill">{{ $category->name }}
                                                    </a>
                                                @endif
                                            @endforeach
                                        </small>
                                    </div>
                                    <small class=" col text-muted">Last updated 3 mins ago</small>
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <div class="mt-2">
            {{ $posts->links() }}
        </div>

    </main>
@endsection
