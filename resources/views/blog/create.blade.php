@extends('layouts.app')

@section('content')
<style>
.ck{
    height: 400px;
}
</style>

    <h1 class="text-center">Create your own Posts Here!!!!</h1>
    <div class="mx-5">
        <div class="card p-3">
            <form method="POST" action="{{ route('store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group has-validation">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" value="{{ old('title') }}" id="title"
                        aria-describedby="titleHelp" placeholder="Title">
                    <small id="titleHelp" class="form-text text-muted">Put here the title of your post</small>

                    @error('title')
                        <p class="text-danger" role="alert">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" name="description" class="form-control" value="{{ old('description') }}"
                        id="description" aria-describedby="descriptionHelp" placeholder="Description">
                    <small id="descriptionHelp" class="form-text text-muted">Small description of your post</small>
                    @error('description')
                        <p class="text-danger" role="alert">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="cover-page">Cover page of your post</label>
                    <input type="file" name="image" class="form-control" id="cover-page" accept="image/*">
                    @error('image')
                        <p class="text-danger" role="alert">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="category">Category</label>
                    <select class="form-control" name="category" aria-describedby="categoryHelp" id="category">
                        <option disabled selected hidden>Choose...</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}"
                                {{ old($category->id) == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                    <small id="categoryHelp" class="form-text text-muted">Choose a category for your post</small>
                    @error('category')
                        <p class="text-danger" role="alert">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tag">Tags</label>
                    <select class="form-control" name="tag[]" aria-describedby="tagHelp" id="tag" multiple="">
                        @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}" {{ old($tag->id) == $tag->id ? 'selected' : '' }}>
                                {{ $tag->name }}</option>
                        @endforeach
                    </select>
                    <small id="tagHelp" class="form-text text-muted">Choose a category for your post</small>
                    @error('tag')
                        <p class="text-danger" role="alert">{{ $message }}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="content">Content of post</label>
                    <textarea id="content" name="content" class="form-control"
                        aria-describedby="contentHelp"> {{ old('content') }}</textarea>
                    <small id="contentHelp" class="form-text text-muted">Small description of your post</small>
                    @error('content')
                        <p class="text-danger" role="alert">{{ $message }}</p>
                    @enderror
                </div>

                <button type="submit" class="btn btn-success mt-2">Create</button>
            </form>
        </div>
    </div>

    <script src="https://cdn.ckeditor.com/ckeditor5/32.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#content'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
