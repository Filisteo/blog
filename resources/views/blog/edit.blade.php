@extends('layouts.app')

@section('content')
    <h1>Edit your post</h1>
    <div class="mx-5">
        <form method="POST" action="{{ route('update', $post) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            @method('put')
            <div class="form-group has-validation">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" value="{{ old('title', $post->title) }}" id="title"
                    aria-describedby="titleHelp" placeholder="Title">
                <small id="titleHelp" class="form-text text-muted">Put here the title of your post</small>

                @error('title')
                    <p class="text-danger" role="alert">{{ $message }}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <input type="text" name="description" class="form-control"
                    value="{{ old('description', $post->description) }}" id="description"
                    aria-describedby="descriptionHelp" placeholder="Description">
                <small id="descriptionHelp" class="form-text text-muted">Small description of your post</small>
                @error('description')
                    <p class="text-danger" role="alert">{{ $message }}</p>
                @enderror
            </div>

            <div class="form-group">
                <label for="cover-page">Cover page of your post</label>
                <input type="file" name="image" class="form-control" id="cover-page" accept="image/*">
                @error('image')
                    <p class="text-danger" role="alert">{{ $message }}</p>
                @enderror
            </div>

            <div class="form-group">
                <label for="category">Category</label>
                <select class="form-control" name="category" aria-describedby="categoryHelp" id="category">
                    <option disabled selected hidden>Choose...</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}"
                            {{ $post->category->id == $category->id ? 'selected' : '' }}>
                            {{ $category->name }}</option>
                    @endforeach
                </select>
                <small id="categoryHelp" class="form-text text-muted">Choose a category for your post</small>
                @error('category')
                    <p class="text-danger" role="alert">{{ $message }}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="tag">Tags</label>
                <select class="form-control select-tag" name="tag[]" aria-describedby="tagHelp" multiple="">
                    @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}" {{ in_array($tag->id, $postTag) ? 'selected' : '' }}>
                            {{ $tag->name }}</option>
                    @endforeach
                </select>
                <small id="tagHelp" class="form-text text-muted">Choose a category for your post</small>
                @error('tag')
                    <p class="text-danger" role="alert">{{ $message }}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="content">Content of post</label>
                <textarea id="content" name="content" class="form-control"
                    aria-describedby="contentHelp"> {{ old('content', $post->content) }}</textarea>
                <small id="contentHelp" class="form-text text-muted">Content of your post</small>
                @error('content')
                    <p class="text-danger" role="alert">{{ $message }}</p>
                @enderror
            </div>
            <button class="btn btn-warning mt-2" data-bs-toggle="modal"
                data-bs-target="#editModal-{{ $post->id }}">Update</button>
        </form>
    </div>

    <script src="https://cdn.ckeditor.com/ckeditor5/32.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#content'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
