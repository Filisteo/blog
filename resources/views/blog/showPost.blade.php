@extends('layouts.app')

@section('content')
    <div class="card mb-3 mx-5">
        <img src="{{ $post->image }}" class="card-img-top" alt="This post don't have cover">
        <div class="card-body">
            <h1 class="card-title text-center border-bottom border-3 border-secondary mb-2 fw-bolder text-secondary">
                {{ $post->title }}</h1>
            <h3 class="card-title fs-4 text mt-4 mx-3">{{ $post->description }}</h3>
            <div class="row">
                <div class="col col-lg-10">
                    <p class="card-text mt-4 mx-3">{!! $post->content !!}</p>
                </div>
                <div class="col bg-secondary">
                    <h4 class="mt-4 text-light">More in: {{$post->category->name}}</h4>
                    @foreach ($similar as $sim)
                    <div class="card border-secondary mb-3" style="max-width: 18rem;">
                        <div class="card-header"><a href="{{route('showPost', $sim)}}">{{$sim->title}}</a></div>
                        <div class="card-body text-secondary">
                            <p class="card-title">{{$sim->description}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <p class="card-text mt-3"><small class="text-muted mx-3">Last updated 3 mins ago</small></p>
            <a class="btn btn-outline-primary mx-3" href="{{ route('blog') }}">Return to posts</a>
        </div>
    </div>
@endsection
