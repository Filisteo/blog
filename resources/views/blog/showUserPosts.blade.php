@extends('layouts.app')

@section('content')
    @if (session('delete'))
        <div class="alert alert-danger">
            {{ session('delete') }}
        </div>
    @endif

    @if (count($posts) == 0)
        <div class="text-center">
            <h1>Don't have post? Create now!!!</h1>
            <a class="btn btn-outline-success mt-2" href="{{ route('create') }}">Go!!</a>
        </div>
    @endif

    @foreach ($posts as $post)
        <div class="container">
            <div class="row">
                <div class="col-4 rounded" style="
                            width: 400px;
                            height:148px;
                            background-image: url({{ $post->image }});
                            background-size: cover;
                            background-repeat:no-repeat">

                </div>
                <div class="col-8">
                    <div class="card mb-3">
                        <div class="card-body">
                            <a href="{{ route('showPost', $post) }}">
                                <h5 class="card-title">{{ $post->title }}</h5>
                            </a>
                            <p class="flex-grow-1 card-text me-5">
                                {{ $post->description }}
                            </p>
                            <p class="card-text">
                            <div class="row">
                                <div class="col">
                                    <a class="btn btn-warning text-light" href="{{ route('edit', $post) }}">
                                        ✏️ Update your post
                                    </a>
                                </div>
                                <div class="col">
                                    <button class="btn btn-danger text-light" data-bs-toggle="modal"
                                        data-bs-target="#deleteModal-{{ $post->id }}">
                                        🗑️ Delete your post
                                    </button>
                                </div>
                            </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.delete-modal')
    @endforeach
@endsection
