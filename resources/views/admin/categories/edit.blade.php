@extends('adminlte::page')


@section('content_header')
    <h1>Edit categories</h1>
@stop

@section('content')

    @if (session('info'))
        <div class="alert alert-success">
            <strong>{{session('info')}}</strong>
        </div>
    @endif

    <div class="card">
        <div class="card-body">
            {{-- De esta manera, cargamos la categoria que previamente creamos, para que laravel collection cree los calores de ese registro  --}}
            {!! Form::model($category, ['route' => ['admin.categories.update', $category], 'method' => 'put']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Category name']) !!}

                @error('name')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            {!! Form::submit('Edit category', ['class' => 'btn btn-warning']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@stop
