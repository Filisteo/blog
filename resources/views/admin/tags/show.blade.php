@extends('adminlte::page')

@section('content_header')
    <h1>Show tag</h1>
@stop

@section('content')

    @if (session('info'))
        <div class="alert alert-danger">
            <strong>{{ session('info') }}</strong>
        </div>
    @endif


    <h1>Tags</h1>
@stop
