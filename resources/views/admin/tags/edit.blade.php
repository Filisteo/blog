@extends('adminlte::page')

@section('content_header')
    <h1>Edit tag</h1>
@stop

@section('content')

    @if (session('info'))
        <div class="alert alert-success">
            <strong>{{ session('info') }}</strong>
        </div>
    @endif
    <div class="card">
        <div class="card-body">
            {{-- De esta manera, cargamos la categoria que previamente creamos, para que laravel collection cree los calores de ese registro  --}}
            {!! Form::model($tag, ['route' => ['admin.tags.update', $tag], 'method' => 'put']) !!}
            @include('admin.tags.partials.form')
            {!! Form::submit('Edit tag', ['class' => 'btn btn-warning']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@stop
