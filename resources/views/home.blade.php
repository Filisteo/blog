@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header fs-3">{{ __('Congratulations') }}</div>

                <div class="card-body">

                    @if (auth()->user()->can('editor') || auth()->user()->can('admin'))
                        <a class="fs-5" href="{{route('blog')}}">Got to blog</a>
                    @else
                        {{ __('You are logged in!, please, access to your email to verify your count') }}
                    @endif

                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection