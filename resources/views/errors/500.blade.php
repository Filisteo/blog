@extends('layouts.app')

@section('content')
<div
class="bg-image"
    style="
    background-image: url('{{asset('img/error-500.jpg')}}');
    height: 100vh;
    background-repeat: no-repeat;">
</div>
@endsection