@extends('layouts.app')

@section('content')
<div
class="bg-image mx-auto"
    style="
    background-image: url('{{asset('img/error-404.jpg')}}');
    height: 100vh;
    background-repeat: no-repeat;">
</div>

@endsection