{{-- Modal confirmacion --}}
<div class="modal fade" id="deleteModal-{{$post->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
aria-labelledby="staticBackdropLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">You're close to deleting the post:
                {{ $post->title }}, are you sure?
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            You have to make a decision about your content, if you want to delete it, press the Delete button, if not,
            press Close button.
        </div>
        <div class="modal-footer">
            <form action="{{ route('destroy', $post) }}" method="POST">
                @csrf
                @method('delete')
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-danger" value="Delete">
            </form>
        </div>
    </div>
</div>
</div>
{{-- Modal confirmacion end --}}