<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/blog') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav me-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ms-auto">
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                    <li>
                        <form action="{{ route('blog') }}" method="GET">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" name="search" class="form-control" placeholder="Search"
                                        value="{{ $search ?? '' }}" />
                                </div>
                                <button type="submit" class="btn btn-primary">
                                    🔎
                                </button>
                            </div>
                        </form>
                    </li>
                @else
                    @can('editor')
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('create') ? 'active bg-success rounded text-light' : '' }}"
                                href="{{ route('create') }}">Create a posts</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('showUserPosts') ? 'active bg-success rounded text-light' : '' }}"
                                href="{{ route('showUserPosts', Auth::user()->name) }}">See your posts</a>
                        </li>
                    @endcan
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Category
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                            <ul class="list-group">
                                @foreach ($categories as $category)
                                    <li class="list-group-item">
                                        <a href="{{ route('category', $category) }}"
                                            class="btn btn-sm rounded-pill">{{ $category->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">

                            {{-- La directiva can verifica si tenemos o no, un permiso --}}
                            @can('admin')
                                <a class="dropdown-item" href="{{ route('admin.home') }}">Dashboard</a>
                            @endcan

                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                                                 document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                    <li>
                        <form action="{{ route('search') }}" method="GET">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" name="search" class="form-control" placeholder="Search"
                                        value="{{ $search ?? '' }}" />
                                </div>
                                <button type="submit" class="btn btn-primary">
                                    🔎
                                </button>
                            </div>
                        </form>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
