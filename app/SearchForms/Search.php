<?php

namespace App\SearchForms;

use App\Models\Post;
use App\Models\Category;

class Search
{

    public static function search($search)
    {
        return Post::where('title', 'LIKE', '%' . $search . '%')
            ->orderBy('created_at', 'desc')
            ->paginate();
    }

    public static function searchCategory($category)
    {
        return Post::where('category_id', $category->id)->orderBy('created_at', 'desc')->paginate();
    }

    public static function searchTag($tag){
        return $tag->posts()->orderBy('created_at', 'desc')->paginate();
    }
}
