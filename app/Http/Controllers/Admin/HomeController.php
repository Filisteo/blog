<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;

class HomeController extends Controller
{
    public function index(){
        $categories = Category::all();
        $posts  = Post::orderBy('created_at', 'desc')->paginate();
        return view('admin.index', compact('posts','categories'));
    }
}
