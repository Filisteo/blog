<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StorePost;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use App\Models\Tag;
use App\SearchForms\Search;

class BlogController extends Controller
{

    //Index es un método que carga la vista principal
    public function index(Request $request)
    {
        //En todos los metodos se cargan las categorias las cuales se añaden a las vistas.
        $categories = Category::all();

        //Almaceno los post paginandolos y ordenandolos por fecha de creaci'on
        $posts = Post::orderBy('created_at', 'desc')->paginate();

        return view('blog.blog', compact('posts', 'categories'));
    }

    /*
    
    -------------------------------------------------- BUSCADORES -----------------------------------------------------------

    Métodos los cuales retornan una reorganización de los posts según la forma de busqueda, la lógica de la busqueda es delegado
    a una facade propia, llamada Search.
    
    */

    // El método search, es un método que recoge la información de un formulario de busqueda, colocado en la vista index.
    public function search(Request $request){
        $categories = Category::all();
        $search = $request->search;

        //En $posts almaceno el resultado que retorna un metódo de una facade propia del proyecto
        $posts = Search::search($search);
        return view('blog.blog', compact('posts', 'search', 'categories'));
    }

    public function tag(Tag $tag){
        $categories = Category::all();
        $posts = Search::searchTag($tag);
        return view('blog.blog', compact('posts', 'categories'));
    }

    public function category(Category $category){
        $categories = Category::all();
        $posts = Search::searchCategory($category);
        return view('blog.blog', compact('posts', 'categories', 'category'));
    }

    /*
    
    -------------------------------------------------- FIN DE: BUSCADORES -----------------------------------------------------------

    */

    /*
    
    -------------------------------------------------- CRUD -----------------------------------------------------------
    En el crud de este controlador, se trabaja la creación, eliminación y edición de los posts

    */


    // El método create, carga la vista con el formulario para crear un post
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('blog.create', compact('categories', 'tags'));
    }

    // El método store, almacena la información que se retorna de los inputs, su validación se delega un archivo, App\Http\Request\StorePost
    // Para ello me aprovecho de las funcionalidades que me otorga el FormRequest 
    public function store(StorePost $request)
    {
        /*
         * La imagen es un elemento opcional de un post, es por ello que los datos se almacenan independientemente de si existe o no. 
         */

        if( $request->file('image') != null){
            $image = $request->file('image')->store('public');
            $url = Storage::url($image);            
            $post = Post::create([
                'user_id' => Auth::id(),
                'category_id' => $request->category,
                'title' => $request->title,
                'description' => $request->description,
                'content' => $request->content,
                'slug' => Str::slug($request->title, '-'),
                'image' => $url,
            ]);
            
            // Para asociar los posts y los tags de dicho post, una relacion N:M, utilizo el método attach, que asocia respectivamente las ids
            $post->tags()->attach($request->tag);
            
        }else{
            $post = Post::create([
                'user_id' => Auth::id(),
                'category_id' => $request->category,
                'title' => $request->title,
                'description' => $request->description,
                'content' => $request->content,
                'slug' => Str::slug($request->title, '-'),
            ]);   

            $post->tags()->attach($request->tag);
        }
        
        //Con el método with, genero una variable de sesión, cuya finalidad es ser leido en la vista, y mostrar una respuesta
        return redirect()->route('blog')->with('success', 'Create a Post');
    }

    //El método showPost, muestra el post que el usuario a seleccionado
    public function showPost(Post $post)
    {
        $categories = Category::all();
        //En la variable similar, almaceno una colección de post, cuya categoría es la misma, los más recientes, y solo obtiene 3
        $similar = Post::where('category_id', $post->category_id)
            ->where('id', '!=', $post->id)
            ->latest('id')
            ->take(3)
            ->get();

        return view('blog.showPost', compact('post','similar','categories'));
    }

    //En el método showUserPosts, retorna los posts de dicho usuario
    public function showUserPosts($name)
    {
        $categories = Category::all();
        //En user obtengo el usuario propio
        $user = User::where('name', $name)->get();
        
        foreach ($user as $key => $value) {
            $user = $value;
        }

        //Con ORM de Eloquent, para asociar la id del autor del post, con el de la id del usuario actual
        $posts  = Post::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();
        return view('blog.showUserPosts', compact('posts','categories'));
    }

    //En el método edit, cargo la vista de edición con los datos del post
    public function edit(Post $post)
    {
        $tags = Tag::all();
        $postTag = array();
        foreach ($post->tags as $tag) {
            array_push($postTag, $tag->id);
        }
        $categories = Category::all();
        return view('blog.edit', compact('post', 'categories','tags', 'postTag'));
    }

    // Update es un método, que actualiza el post.
    public function update(StorePost $request, Post $post)
    {

        if( $request->file('image') != null){

            $image = $request->file('image')->store('public');
            $url = Storage::url($image);

            $post->update([
                'category_id' => $request->category,
                'title' => $request->title,
                'description' => $request->description,
                'content' => $request->content,
                'slug' => Str::slug($request->title, '-'),
                'image' => $url,
            ]);
            //El método sync reemplaza los tags anteriores por los nuevos  
            $post->tags()->sync($request->tag); 
        }else{
            $post->update([
                'category_id' => $request->category,
                'title' => $request->title,
                'description' => $request->description,
                'content' => $request->content,
                'slug' => Str::slug($request->title, '-'),
            ]);
            $post->tags()->sync($request->tag);     
        }

        return redirect()->route('blog')->with('updated', 'The post was updated');
    }

    // El método destroy elimina posts
    public function destroy(Post $post)
    {
        $post->delete();
        return back()->with('delete', 'The post was deleted');
    }

    /*
    
    -------------------------------------------------- FIN DE: CRUD -----------------------------------------------------------
    
    */
}
