<?php

namespace App\Providers;

use App\SearchForms\Search;
use Illuminate\Support\ServiceProvider;


class SearchServiceProvide extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('search', function(){
            return new Search();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
