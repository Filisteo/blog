<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  use HasFactory;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','slug'];

    //Cambia la clave, la usualmente utilizada de la ruta(id) por la propiedad slug
    public function getRouteKeyName()
    {
      return "slug";
    }


    public function posts()
    {
        return $this->hasMany('App\Models\Post', 'category_id', 'id');
    }
    

}
