<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;


class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'category_id', 'title', 'description', 'topic', 'content', 'slug', 'image'
    ];
    
    protected function title(): Attribute
    {
        return new Attribute(
            get: fn ($value) =>  ucwords($value),
            set: fn ($value) => strtolower($value),
        );
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }


    //RELACION UNO A MUCHOS INVERSA

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }


    //RELACION MUCHOS A MUCHOS

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }
}
